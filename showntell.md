# Show-and-tell notes

### Locaties
* Laat aarde met locaties/magnitude bevingen zien. 

* Niet-aardbevingen.

### Magnitude 
* Magnitude histogram.

### Oudere data op zwakkere aardbeving

* Histogram laat zien dat zwakkere aardbevingen pas later in data zitten.

  Waarschijnlijk omdat het eerder minder snel geregistreerd werd.

* Kan nog veel meer gedaan worden om patronen van verbeterde registratie
  te vinden.

* In een filmpje van de eerde hele-aarde-plot zou het ook zichtbaar kunnen
  zijn.

### Diepte
* Oudere data mist diepere bevingen.

* Diepte vs magnitude histogram.

  + Zelf logirithmisch gedaan, lijkt dat `seaborn`/`pyplot`/`pandas` het
    niet goed kunnen?

  + `plt.hist2d` return bevat een 2D array die we hebben verwerkt.
    Onhandig de `plt.hist2d` plot weer weg gewerkt.

### Haiti en Katmandu

* Opvolgende dagen veel activiteit. Plot log10(dagen) gemaakt.

  Maar een beetje "ruw".

* De selectie gecheckt op de kaart zodat andere locaties er niet bij vallen.
  De check was nodig, bijvoorbeeld `"paris"`.

* Tijden in deze dataset zijn super-irritant en spelen niet goed samen met
  de plotters :/

### Andere grote aardbevingen

* Hebben een lijst gemaakt van andere grote aardbevingen.

  + Wel nogal trage code hier.

* Nog  niet een plot hiervan gemaakt. *TODO*

### Astropy

* Handig om libraries te hebben.

* Is wel een zooitje, kon niet vinden wat ik zocht..

### Ephem

* Een stuk beter, maar doet veel minder.

* Leek geen info over de orientatie van objecten te hebben behalve Saturnus,
  de Maan.

* Kon toch nog data orientatie uit trekken door het te laten berekenen vanuit
  twee observers aan de overkant op de evenaar.

  (.. waarschijnlijk & hopelijk)

* *TODO* 

### Relatie relative positie en (triggeren)aardbevingen?

* *TODO*
