import timeit

import pandas
from dateutil import parser

from pathlib import Path
from urllib import request

def raw_get_query(file, args):
    url = 'https://earthquake.usgs.gov/fdsnws/event/1/query?' + args
    print(url)
    r = request.urlopen(url)
    if r.getcode() == 200:
        with open(file, 'w') as fd:
            for line in r.readlines():
                fd.write(line.decode())# + "\n")

    r.close()

    
def load_file(file):
    got = pandas.read_csv(file)
    got.time = [parser.parse(time) for time in got.time]
    return got

def get_query(year, month, min_mag=3.5):
    file = "data/{}-{}.csv".format(year, month)
    if not Path(file).exists():
        print('obtain', file)
        next_month, maybe_next_year = month + 1, year
        if next_month > 12:
            maybe_next_year = year + 1
            next_month = 1
        fmt_str = "format=csv&starttime={}-{}-01&endtime={}-{}-01&minmagnitude={}"
        arg = fmt_str.format(year, month, maybe_next_year, next_month, min_mag)
        raw_get_query(file, arg)

    return load_file(file)

def month_range(from_year, from_month, to_year, to_month):
    y,m = from_year, from_month
    while y < to_year:
        while m <= 12:
            yield(y, m)
            m = m + 1
        m = 1
        y = y +1

    if y == to_year:
        while m < to_month:
            yield(y,m)
            m = m + 1

def time_whine(gen, interval=10, whiny='i got'):
    next_whine = timeit.time.time() + interval
    for got in gen:
        if next_whine < timeit.time.time():
            print(whiny, got)
            next_whine = timeit.time.time() + interval
        yield got

def get_time_range(month_generator, prep=lambda x: x):
    return pandas.concat([prep(get_query(year, month))
                          for year,month in month_generator])
