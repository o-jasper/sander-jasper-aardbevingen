# Aardbevingen opdracht Jasper en Sander
Data die we zouden willen hebben:

* Kaart van de Aarde/breuklijnen.
* Aardbevingen: magnitude, tijd,(duratie) locatie.

## Data bronnen/libraries

### [Geopandas](https://gitlab.com/anchormen/high-potential-program/blob/master/example-code/geopandas-mapping.ipynb)

### [De gegeven API](https://earthquake.usgs.gov/fdsnws/event/1/)

## Opdracht B: Aardbevingen
Maak een analyse  in een Jupyter Notebook dat (in elk geval) de volgende vragen 
probeert te beantwoorden:

* Waar komen over het algemeen de meeste aardbevingen voor, op het noordelijk
  of het zuidelijk halfrond?
  + Histogram latitude.
  + gemiddelde latitude.(hoger/lager evenaar)
  + (kaartje)
  + Niet te vroeg/lage magnitude (false negatives)
* Op welke diepte komen de meeste aardbevingen voor?
  + Histogram diepte.
* Kun je een verband ontdekken tussen diepte en magnitude?
  + Scatterplot/kde/2d histogram.
* Is het aantal aardbevingen(per maand of per jaar)
  rond Haiti flink toegenomen sinds 2010?
  + Definieer "rond Haiti".. wellicht `place` rij.
* En rond Kathmandu(Nepal) sinds 2015?
  + idem
* Eventueel: andere aardbevingsgrelateerde vragen die je zelf hebt

Datasets/API’s die je hiervoor zou kunnen gebruiken:

* USGS Earthquake Data API: https://earthquake.usgs.gov/fdsnws/event/1/
  + Voorbeeld: 
    https://earthquake.usgs.gov/fdsnws/event/1/query?format=csv&starttime=2014-01-01&endtime=2014-01-02&minmagnitude=4

## Andere (potentiele)doelen

* Heeft de zwaartekracht gradient van de maan/aarde en/of de getijden een
  invloed? (bijv [pa\op-sci artikel](https://www.livescience.com/55448-how-tides-trigger-san-andreas-earthquakes.html))
* Als er een serie aardbeving plaatsvind, is er een algemeen patroon?

Info hiervoor:

* Getijden/zwaartekracht gradient over de tijd.
